﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace REx
{
    /// <summary>
    /// Class which represents a directed Graph.
    /// </summary>
    public class Graph : IEquatable<Graph>
    {
        private List<Edge> Edges = new List<Edge>();
        private List<Node> Nodes = new List<Node>();

        /// <summary>
        /// Constructor with one Node.
        /// </summary>
        /// <param name="node">Node of the graph.</param>
        public Graph(Node node)
        {
            if (node != null)
            {
                Nodes.Add(node);
            }
        }

        /// <summary>
        /// Creats a graph with nodes.
        /// </summary>
        /// <param name="nodes">List of Nodes.</param>
        public Graph(List<Node> nodes)
        {
            foreach (Node node in nodes)
            {
                addNode(node);
            }
        }

        /// <summary>
        /// Creates a graph with a nodes and edges.
        /// </summary>
        /// <param name="nodes">List of nodes</param>
        /// <param name="edges">List of edges</param>
        public Graph(List<Node> nodes, List<Edge> edges)
        {
            foreach (Node node in nodes)
            {
                addNode(node);
            }
            foreach (Edge edge in edges)
            {
                addEdge(edge);
            }
        }

        /// <summary>
        /// Returns the node with the given Id.
        /// </summary>
        /// <param name="NodeID">Id of the requested node</param>
        public Node getNodeById(int NodeID)
        {
            Node tempNode = null;
            foreach (Node node in Nodes)
            {
                if (node.Id == NodeID)
                {
                    tempNode = node;
                }
            }
            return tempNode;
        }

        /// <summary>
        /// Adds a node to the graph
        /// </summary>
        /// <param name="_addNode">Node which will be added</param>
        public void addNode(Node _addNode)
        {
            if (_addNode != null)
            {
                if (Nodes.Contains(_addNode)) //check if ID is identical
                {
                    int nodeIndex = Nodes.IndexOf(_addNode);
                    if (Nodes[nodeIndex].Label == "") //check for missing label information
                    {
                        Nodes[nodeIndex] = _addNode; //overwrite with newer data
                    }
                    foreach (Edge edge in Edges) //update edges with same information
                    {
                        if (edge.Source.Equals(Nodes[nodeIndex]))
                        {
                            edge.Source = Nodes[nodeIndex];
                        }
                        if (edge.Destination.Equals(Nodes[nodeIndex]))
                        {
                            edge.Destination = Nodes[nodeIndex];
                        }
                    }
                }
                else
                {
                    Nodes.Add(_addNode);
                }
            }
        }

        /// <summary>
        /// Returns all nodes of the graph.
        /// </summary>
        public List<Node> getAllNodes()
        {
            return Nodes;
        }

        /// <summary>
        /// Get all nodes which have no incomming edge (Edge of a Node which has no source). 
        /// </summary>
        /// <returns>List of nodes</returns>
        public List<Node> getTerminalNodes()
        {
            List<Node> res = new List<Node>(this.Nodes);
            foreach (Edge edge in this.Edges)
            {
                if (edge.Source != null)
                {
                    res.Remove(edge.Source);
                }
            }
            return res;
        }

        /// <summary>
        /// Returns the edge with the given Id.
        /// </summary>
        /// <param name="EdgeId">KantenID der gewünschten Kante.</param>
        public List<Edge> getEdgesById(int EdgeId)
        {
            List<Edge> res = new List<Edge>();
            foreach (Edge edge in Edges)
            {
                if (edge.Id == EdgeId)
                {
                    res.Add(edge);
                }
            }
            return res;
        }

        /// <summary>
        /// Adds a edge to the graph.
        /// </summary>
        /// <param name="_addEdge">Edge which will be added</param>
        public void addEdge(Edge _addEdge)
        {
            if (Edges.Contains(_addEdge))
            {
                int edgeIndex = Edges.IndexOf(_addEdge);
                if (Edges[edgeIndex].Label == "") //check for missing label information
                {
                    Edges[edgeIndex] = _addEdge;
                }
            }
            else
            {
                Edges.Add(_addEdge);
            }

            // check if updated node information is available
            if (_addEdge.Source != null)
            {
                addNode(_addEdge.Source);
            }
            if (_addEdge.Destination != null)
            {
                addNode(_addEdge.Destination);
            }
        }

        /// <summary>
        /// Get all edges of the graph.
        /// </summary>
        /// <returns></returns>
        public List<Edge> getAllEdges()
        {
            return Edges;
        }

        /// <summary>
        /// Get all ingoing edges of a node.
        /// </summary>
        /// <param name="Node"></param>
        public List<Edge> getInEdges(Node Node)
        {
            List<Edge> ingoingEdges = new List<Edge>();
            foreach (Edge edge in Edges)
            {
                if (edge.Destination.Equals(Node))
                {
                    ingoingEdges.Add(edge);
                }
            }
            return ingoingEdges;
        }

        /// <summary>
        /// Get all outgoing edges of a node.
        /// </summary>
        /// <param name="_node"></param>
        public List<Edge> getOutEdges(Node _node)
        {
            List<Edge> outgoingEdges = new List<Edge>();
            foreach (Edge edge in Edges)
            {
                if (edge.Source.Equals(_node))
                {
                    outgoingEdges.Add(edge);
                }
            }
            return outgoingEdges;
        }

        /// <summary>
        /// Joins two graphs to on graph. The new graph does not have to be a connected graph.
        /// </summary>
        /// <param name="_joinGraph"></param>
        public void joinGraph(Graph _joinGraph)
        {
            foreach (Node joinNode in _joinGraph.getAllNodes())
            {
                this.addNode(joinNode);
            }
            foreach (Edge joinEdge in _joinGraph.getAllEdges())
            {
                this.addEdge(joinEdge);
            }
        }

        /// <summary>
        /// Checks if the graph is disconnected (treu) or not (false).
        /// </summary>
        public bool IsDisconnected
        {
            get
            {
                return checkIsDisconnected();
            }

            private set
            {
                IsDisconnected = value;
            }
        }

        /// <summary>
        /// Checks if the graph gots unconnected edges
        /// </summary>
        private bool checkIsDisconnected()
        {
            if (Edges != null && Edges.Count != 0)
            {
                bool[] visited = new bool[Nodes.Count()];
                for (int i = 0; i < visited.Length; i++)
                {
                    visited[i] = false;
                }
                Stack nodesToCheck = new Stack();
                Edge firstEdge = Edges[0];
                nodesToCheck.Push(firstEdge.Destination);
                nodesToCheck.Push(firstEdge.Source);
                visited[Nodes.IndexOf(firstEdge.Source)] = true;
                visited[Nodes.IndexOf(firstEdge.Destination)] = true;
                while (nodesToCheck.Count != 0)
                {
                    Node tempNode = (Node)nodesToCheck.Pop();
                    List<Edge> tempEdges = getInEdges(tempNode);
                    foreach (Edge edge in tempEdges)
                    {
                        int indexOfDestination = Nodes.IndexOf(edge.Destination);
                        if (visited[indexOfDestination] != true)
                        {
                            visited[indexOfDestination] = true;
                            nodesToCheck.Push(edge.Destination);
                        }
                        int indexOfSource = Nodes.IndexOf(edge.Source);
                        if (visited[indexOfSource] != true)
                        {
                            visited[indexOfSource] = true;
                            nodesToCheck.Push(edge.Source);
                        }
                    }
                }
                if (visited.Contains(false))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        /// <summary>
        /// Equals-comparison function.
        /// </summary>
        /// <param name="_compGraph"></param>
        /// <returns></returns>
        public bool Equals(Graph _compGraph)
        {
            List<Node> uncheckedNodes = Nodes.ToList();
            List<Edge> uncheckedEdges = Edges.ToList();
            foreach (Node compNode in _compGraph.getAllNodes())
            {
                if (uncheckedNodes.Contains(compNode))
                {
                    uncheckedNodes.Remove(compNode);
                }
                else
                {
                    return false;
                }
            }
            if (uncheckedNodes.Count > 0)
            {
                return false;
            }
            foreach (Edge compEdge in _compGraph.getAllEdges())
            {
                if (uncheckedEdges.Contains(compEdge))
                {
                    uncheckedEdges.Remove(compEdge);
                }
                else
                {
                    return false;
                }
            }
            if (uncheckedEdges.Count > 0)
            {
                return false;
            }
            return true;
        }
    }
}
