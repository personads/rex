﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx
{
    //TODO: falls nicht genutzt entfernen!
    class GraphBinding
    {
        private Graph baseGraph;

        public Node RootNode
        {
            get { return baseGraph.getAllNodes()[0]; }
            private set { }
        }

        public List<Edge> Edges
        {
            get { return baseGraph.getAllEdges(); }
            private set { }
        }

        public List<Tuple<Edge, List<Node>>> NodesByEdges
        {
            get { return getNodesByEdges(); }
            private set { }
        }

        public GraphBinding(Graph _baseGraph)
        {
            baseGraph = _baseGraph;
        }

        private List<Tuple<Edge, List<Node>>> getNodesByEdges()
        {
            List<Tuple<Edge, List<Node>>> res = new List<Tuple<Edge, List<Node>>>();
            Dictionary<int, List<Node>> resDict = new Dictionary<int, List<Node>>();
            foreach (Edge curEdge in baseGraph.getAllEdges())
            {
                if (!resDict.ContainsKey(curEdge.Id))
                {
                    resDict[curEdge.Id] = new List<Node>();
                }
                resDict[curEdge.Id].Add(curEdge.Destination);
            }
            foreach (KeyValuePair<int, List<Node>> entry in resDict)
            {
                Edge resEdge = baseGraph.getEdgesById(entry.Key)[0];
                res.Add(new Tuple<Edge, List<Node>>(resEdge, entry.Value));
            }
            return res;
        }
    }
}
