﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REx
{
    /// <summary>
    /// Class which represents a Edge
    /// </summary>
    public class Edge : IEquatable<Edge>
    {
        /// <summary>
        /// Constructor for a Edge with a source and a destination Node.
        /// </summary>
        /// <param name="id">Id of the Edge.</param>
        /// <param name="label">(optional) Label of Edge.</param>
        /// <param name="source">(optional) Source Node of the Edge</param>
        /// <param name="destination">(Optional) Destination Node of the Edge.</param>
        public Edge(int id, Node source = null, Node destination = null, string label = "")
        {
            Id = id;
            Label = label;
            Source = source;
            Destination = destination;
        }

        /// <summary>
        /// Id of Edge.
        /// </summary>
        public int Id
        {
            get; private set;
        }

        /// <summary>
        /// Label of Edge.
        /// </summary>
        public string Label
        {
            get; private set;
        }

        /// <summary>
        /// Source Node of Edge
        /// </summary>
        public Node Source
        {
            get; set;
        }

        /// <summary>
        /// Destination Node of Edge
        /// </summary>
        public Node Destination
        {
            get; set;
        }

        /// <summary>
        /// Equals-comparison function.
        /// </summary>
        /// <param name="_compEdge">Edge which will be compared withe the current Edge</param>
        /// <returns></returns>
        public bool Equals(Edge _compEdge)
        {
            return (this.Id == _compEdge.Id) && (this.Source.Equals(_compEdge.Source)) && (this.Destination.Equals(_compEdge.Destination));
        }

        /// <summary>
        /// Hashsum function (Needs to be overriden for correct Dictionary searchings)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string res = "P" + Id;
            if (Label != "")
            {
                res = Label + " (P" + Id + ")";
            }
            return res;
        }
    }
}
