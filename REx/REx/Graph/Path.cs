﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx
{
    // TODO: Ungenutzte Funktionen entfernen?

    /// <summary>
    /// Class which represents a Path in a Graph.
    /// </summary>
    class Path : IEquatable<Path>
    {
        /// <summary>
        /// Path as a list of edges
        /// </summary>
        public List<Edge> Edges { get; private set; }

        /// <summary>
        /// Constructor to create a inital path.
        /// </summary>
        /// <param name="_edges">(optional) List of edges which represents the path or a part of it</param>
        public Path(List<Edge> _edges = null)
        {
            Edges = _edges ?? new List<Edge>();
        }
        // TODO: remove?
        public void addEdge(Edge _edge)
        {
            Edges.Add(_edge);
        }

        /// <summary>
        /// Insertes a Edge to the path at a specified position
        /// </summary>
        /// <param name="_position"></param>
        /// <param name="_edge"></param>
        public void insertEdge(int _position, Edge _edge)
        {
            Edges.Insert(_position, _edge);
        }
        // TODO: remove?
        public Node getSource()
        {
            Node res = null;
            if (Edges.Count > 0)
            {
                res = Edges[0].Source;
            }
            return res;
        }
        // TODO: remove?
        public Node getDestination()
        {
            Node res = null;
            if (Edges.Count > 0)
            {
                res = Edges[Edges.Count-1].Destination;
            }
            return res;
        }
        /// <summary>
        /// Get all nodes which are attached by the path.
        /// (source and destination node of every edge in the path)
        /// </summary>
        /// <returns></returns>
        public List<Node> getNodes()
        {
            List<Node> res = new List<Node>();
            foreach (Edge e in Edges)
            {
                res.Add(e.Source);
                res.Add(e.Destination);
            }
            return res;
        }
        // TODO: remove?
        public bool coversNode(Node _node)
        {
            bool res = false;
            res = getNodes().Contains(_node);
            return res;
        }
        // TODO: remove?
        public bool coversNodes(List<Node> _nodes)
        {
            bool res = false;
            HashSet<Node> pathNodes = new HashSet<Node>(getNodes());
            HashSet<Node> compNodes = new HashSet<Node>(_nodes);
            res = pathNodes.SetEquals(compNodes);
            return res;
        }
        // TODO: remove?
        public bool coversPath(Path _path)
        {
            bool res = true;
            for (int i = 0; i < Edges.Count; i++)
            {
                if (i < _path.Edges.Count)
                {
                    if (!Edges[i].Equals(_path.Edges[i]))
                    {
                        res = false;
                        break;
                    }
                } else
                {
                    break;
                }
            }
            if ( (res) && (_path.Edges.Count > Edges.Count))
            {
                res = false;
            }
            return res;
        }
        /// <summary>
        /// Equals-comparison function.
        /// </summary>
        /// <param name="_compPath"></param>
        /// <returns></returns>
        public bool Equals(Path _compPath)
        {
            bool res = true;
            // check for equal length
            if (Edges.Count == _compPath.Edges.Count)
            {
                // check for equal ordering
                for (int i = 0; i < Edges.Count; i++)
                {
                    // check for equal Edges
                    if (!Edges[i].Equals(_compPath.Edges[i]))
                    {
                        res = false;
                        break;
                    }
                }
            }
            else
            {
                res = false;
            }
            return res;
        }
        /// <summary>
        /// Hashsum function (Needs to be overriden for correct Dictionary searchings)
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            var result = 0;
            result = (result * 397) ^ Edges.GetHashCode();
            return result;
        }
    }
}
