﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REx
{
    /// <summary>
    /// Class which represents a node (of a graph).
    /// </summary>
    public class Node : IEquatable<Node>
    {
        /// <summary>
        /// Constructor with for one node.
        /// </summary>
        /// <param name="id">Id of node.</param>
        /// <param name="label">(otional) Label of node.</param>
        public Node(int id, string label="")
        {
            Id = id;
            Label = label;
        }

        /// <summary>
        /// Id of node.
        /// </summary>
        public int Id
        {
            get; set;
        }

        /// <summary>
        /// Labeld of node
        /// </summary>
        public string Label
        {
            get; set;
        }

        /// <summary>
        /// Equals-comparison function.
        /// </summary>
        /// <param name="_compNode"></param>
        /// <returns></returns>
        public bool Equals(Node _compNode)
        {
            return (this.Id == _compNode.Id);
        }

        /// <summary>
        /// Retrun the node as a string with the Label and the Id in braces (with leading "Q")
        /// e.g.: Copper (Q753)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string res = "Q" + Id;
            if (Label != "")
            {
                res = Label + " (Q" + Id + ")";
            }
            return res;
        }
    }
}
