﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx
{
    /// <summary>
    /// Class with static functions for graphs.
    /// </summary>
    static class GraphUtils
    {
        /// <summary>
        /// Joins several graphs to one big graph.
        /// </summary>
        /// <param name="graphList">List of graphs which will be joined</param>
        /// <returns>Einen einziegen großen Graph</returns>
        public static Graph joinGraphs(List<Graph> graphList)
        {
            Graph graph = graphList.First();

            if (graphList.Count > 1)
            {
                for (int i = 1; i < graphList.Count; i++)
                {
                    graph.joinGraph(graphList[i]);
                }
            }
            return graph;
        }

        //TODO: was für Combinationen werden hier erstellt?
        public static List<List<T>> getAllCombinations<T>(List<List<T>> _possibleElements)
        {
            List<List<T>> res = new List<List<T>>();
            if (_possibleElements.Count <= 1)
            {
                foreach (T curElement in _possibleElements.First())
                {
                    res.Add(new List<T> { curElement });
                }
            }
            else
            {
                for (int i = 0; i < _possibleElements.Count; i++)
                {
                    foreach (T curElement in _possibleElements[i])
                    {
                        List<List<T>> otherElements = new List<List<T>>(_possibleElements);
                        otherElements.RemoveAt(i);
                        foreach (List<T> otherCombination in getAllCombinations(otherElements))
                        {
                            List<T> newCombination = new List<T>(otherCombination);
                            newCombination.Add(curElement);
                            if (!containsCombination(res, newCombination))
                            {
                                newCombination.Reverse();
                                res.Add(newCombination);
                            }
                        }
                    }
                }
            }
            return res;
        }
        // TODO: beschreibung erstellen (was wird hier combiniert)?
        // TODO: auskommentierten Code entfernen?!
        public static List<List<Path>> getPathCombinations(Dictionary<Node, List<Path>> _possiblePaths)
        {
            List<List<Path>> res = new List<List<Path>>();
            if (_possiblePaths.Keys.Count <= 1)
            {
                res = _possiblePaths.Values.ToList();
            }
            else
            {
                foreach (Node sourceNode in _possiblePaths.Keys)
                {
                    Dictionary<Node, List<Path>> otherPossiblePaths = new Dictionary<Node, List<Path>>(_possiblePaths);
                    otherPossiblePaths.Remove(sourceNode);
                    foreach (Path curPath in _possiblePaths[sourceNode])
                    {
                        foreach (List<Path> curCombination in getPathCombinations(otherPossiblePaths))
                        {
                            List<Path> newCombination = new List<Path>(curCombination);
                            // check if current path is already covered by path in current combination
                            bool curPathCovered = false;
                            //foreach (Path curCombinationPath in curCombination)
                            //{
                            //    if (curCombinationPath.coversPath(curPath))
                            //    {
                            //        curPathCovered = true;
                            //        break;
                            //    }
                            //    // if the current path covers a previous path, it should replace the latter
                            //    else if (curPath.coversPath(curCombinationPath))
                            //    {
                            //        newCombination.Remove(curCombinationPath);
                            //    }
                            //}
                            if (!curPathCovered)
                            {
                                // if the current path is not yet covered, it should be added
                                newCombination.Add(curPath);
                            }
                            // check for duplicates
                            if (!containsCombination(res, newCombination))
                            {
                                // and add new combination
                                res.Add(newCombination);
                            }
                        }
                    }
                }
            }
            return res;
        }
        /// <summary>
        /// Check if a combination in a list is in a other list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_container"></param>
        /// <param name="_combination"></param>
        /// <returns></returns>
        private static bool containsCombination<T>(List<List<T>> _container, List<T> _combination)
        {
            bool res = false;
            foreach (List<T> contCombination in _container)
            {
                bool combinationEqual = true;
                foreach (T contCombinationItem in contCombination)
                {
                    if (!_combination.Contains(contCombinationItem))
                    {
                        combinationEqual = false;
                        break;
                    }
                }
                if (combinationEqual)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }
    }
}
