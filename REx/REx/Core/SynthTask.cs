﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx.Core
{
    class SynthTask
    {
        // Value which describe the Number of retrieved Graphs.
        // It limeted the later showen Graphs in the UI
        public const int NumberOfRetrievedGraphs = 3;

        /// <summary>
        /// The Query from the Userinput
        /// </summary>
        public string[] Query
        {
            get; set;
        }

        /// <summary>
        /// List of Graphs which are the basis for the Input
        /// </summary>
        public List<List<Graph>> BaseGraphs
        {
            get; set;
        }

        /// <summary>
        /// List of all Stencils which was founded.
        /// </summary>
        public List<List<Path>> Stencils
        {
            get; set;
        }

        /// <summary>
        /// List of resulting Graphs of the Executen step
        /// </summary>
        public List<List<Graph>> ResultGraphSets
        {
            get; set;
        }

        public int MaxDepth
        {
            get; set;
        }

        private IProgress<string> WorkingProgress
        {
            get; set;
        }

        private IProgress<string> LogProgress
        {
            get; set;
        }

        /// <summary>
        /// Create a new SyntTask.
        /// </summary>
        /// <param name="_query"></param>
        /// <param name="_baseGraphs"></param>
        /// <param name="_stencils"></param>
        /// <param name="_resultGraphs"></param>
        /// <param name="_maxDepth"></param>
        /// <param name="_workingProgress"></param>
        /// <param name="_logProgress"></param>
        public SynthTask(string[] _query, List<List<Graph>> _baseGraphs = null, List<List<Path>> _stencils = null, List<List<Graph>> _resultGraphs = null, int _maxDepth = 1, IProgress<string> _workingProgress = null, IProgress<string> _logProgress = null)
        {
            Query = _query;
            BaseGraphs = _baseGraphs ?? new List<List<Graph>>();
            Stencils = _stencils ?? new List<List<Path>>();
            ResultGraphSets = _resultGraphs ?? new List<List<Graph>>();
            MaxDepth = 1;
            WorkingProgress = _workingProgress;
            LogProgress = _logProgress;
        }

        public async Task<List<Graph>> getResultGraphs()
        {
            List<Graph> res = new List<Graph>();
            foreach (List<Graph> resultGraphSet in await Execute())
            {
                res = res.Concat(resultGraphSet).ToList();
            }
            return res;

        }

        /// <summary>
        /// Get all Results from the Execution
        /// </summary>
        /// <returns>List of SynthResults</returns>
        public async Task<List<SynthResult>> getSynthResults()
        {
            List<SynthResult> res = new List<SynthResult>();
            if (ResultGraphSets.Count < 1)
            {
                await Execute();
            }
            for (int s = 0; s < ResultGraphSets.Count; s++)
            {
                foreach (Graph resultGraph in ResultGraphSets[s])
                {
                    res.Add(new SynthResult(resultGraph, Stencils[s], s));
                }
            }
            return res;
        }

        /// <summary>
        /// Asynchron Task to Run a complet synthesis process
        /// </summary>
        /// <returns>A List of Graphs whiche was founded by the synthesis task</returns>
        public async Task<List<List<Graph>>> Execute()
        {
            API.WikiData wd = new API.WikiData();
            API.WikiDataQuery wdq = new API.WikiDataQuery();

            // Create new BaseGraphs by the given Input
            if (BaseGraphs.Count == 0)
            {
                reportProgess("Searching WikiData...");
                // get list of result IDs for each search term
                List<List<string>> resultSets = new List<List<string>>();
                for (int i=0; i < Query.Length; i++)
                {
                    // iterate over query terms
                    string searchTerm = Query[i];
                    resultSets.Add(new List<string>());
                    // retrieve result root nodes (limit = 10)
                    List<Node> curResults = await wd.searchEntities(searchTerm);
                    // if there are results
                    if (curResults.Count > 0)
                    {
                        foreach (Node curResult in curResults)
                        {
                            // add result at corresponding query index
                            resultSets[i].Add("Q" + curResult.Id);
                        }
                        reportProgess("Retrieved " + curResults.Count + " results for '" + searchTerm + "'");
                    }
                    else
                    {
                        reportProgess("No results were found for '" + searchTerm + "'");
                        return ResultGraphSets;
                    }
                }
                reportProgess("");

                reportProgess("Retrieving Graphs...");
                int totalGraphCount = 0;
                // iterate over result ID sets
                foreach (List<string> resultSet in resultSets)
                {
                    List<Graph> wdEntities = await wd.getEntities(resultSet);
                    // add result graphs at term index
                    BaseGraphs.Add(wdEntities);
                    totalGraphCount += wdEntities.Count;
                }
                reportProgess("A total of " + totalGraphCount + " graphs were retrieved");
                // reset stencils
                Stencils.Clear();
            }
            
            // Find all Stencils from the BaseGraphs
            if (Stencils.Count == 0)
            {
                reportProgess("");
                reportProgess("Finding Stencils...");
                foreach (List<Graph> BaseGraphSet in GraphUtils.getAllCombinations(BaseGraphs))
                {
                    //Stencils = Stencils.Concat(SynthEngine.findStencils(BaseGraphSet)).ToList();
                    // TODO: find better recombination
                    // break on first result set for which a stencil can be found
                    Stencils = SynthEngine.findStencils(BaseGraphSet);
                    if (Stencils.Count > 0)
                    {
                        break;
                    }
                }
                reportProgess(Stencils.Count + " stencils were found");
                for (int i = 0; i < Stencils.Count; i++)
                {
                    reportProgess("[" + i + "] has the following paths:");
                    foreach (Path path in Stencils[i])
                    {
                        string pathString = "";
                        foreach (Edge edge in path.Edges)
                        {
                            pathString += "[Q" + edge.Source.Id + "]? -P" + edge.Id + "-> [Q" + edge.Destination.Id + "]? ";
                        }
                        reportProgess(pathString);
                    }
                    reportProgess("");
                }
                if (Stencils.Count < 1)
                {
                    reportProgess("No stencils were found");
                    return ResultGraphSets;
                }
                // reset results
                ResultGraphSets.Clear();
            }

            // Create the Query for the Stencils
            reportProgess("");
            reportProgess("Synthesizing Queries...");
            List<string> wdqQueries = new List<string>();
            foreach (List<Path> stencil in Stencils)
            {
                wdqQueries.Add(SynthEngine.synthQuery(stencil));
                reportProgess("The following WDQ-Query was synthesized: '" + wdqQueries.Last() + "'");
            }

            // Find all ResultGraphs from the synthesis task
            if (ResultGraphSets.Count == 0)
            {
                reportProgess("");
                reportProgess("Searching WikiDataQuery...");
                List<List<int>> wdqResultSets = new List<List<int>>();
                foreach (string wdqQuery in wdqQueries)
                {
                    wdqResultSets.Add(await wdq.getEntities(wdqQuery));
                    reportProgess(wdqResultSets.Last().Count + " entities matching the stencil were retrieved");
                }

                reportProgess("");
                foreach (List<int> wdqResultSet in wdqResultSets)
                {
                    reportProgess("Retrieving Graphs for first " + NumberOfRetrievedGraphs+ " entities of stencil...");
                    List<string> resultIds = new List<string>();
                    for (int i = 0; i < NumberOfRetrievedGraphs; i++)
                    {
                        if (i < wdqResultSet.Count)
                        {
                            resultIds.Add("Q" + wdqResultSet[i]);
                        }
                    }
                    ResultGraphSets.Add(await wd.getEntities(resultIds));
                    reportProgess(ResultGraphSets.Last().Count + " graphs retrieved");
                }

                reportProgess("");
                reportProgess("Applying stencil to entities...");
                for (int i = 0; i < ResultGraphSets.Count; i++)
                {
                    for (int j = 0; j < ResultGraphSets[i].Count; j++)
                    {
                        ResultGraphSets[i][j] = await SynthEngine.applyStencil(ResultGraphSets[i][j], Stencils[i]);
                    }
                    // remove null results
                    ResultGraphSets[i].RemoveAll(result => result == null);
                    reportProgess("Stencil successfullly applied to " + ResultGraphSets[i].Count + " graphs");
                }

                reportProgess("");
                reportProgess("Resolving entities...");
                for (int i = 0; i < ResultGraphSets.Count; i++)
                {
                    for (int j = 0; j < ResultGraphSets[i].Count; j++)
                    {
                        ResultGraphSets[i][j] = await SynthEngine.labelGraph(ResultGraphSets[i][j]);
                        reportProgess("\n" + i + ": '" + ResultGraphSets[i][j].getAllNodes()[0].Label + "' (Q" + ResultGraphSets[i][j].getAllNodes()[0].Id + ")");
                        reportProgess("'" + ResultGraphSets[i][j].getAllNodes()[0].Label + "' (Q" + ResultGraphSets[i][j].getAllNodes()[0].Id + ")");
                        foreach (Edge resEdge in ResultGraphSets[i][j].getAllEdges())
                        {
                            reportProgess(resEdge.Source.ToString() + " -P" + resEdge.Id + "-> " + resEdge.Destination.ToString());
                        }
                    }
                }
            }

            return ResultGraphSets;
        }

        private void reportProgess(string _progressString)
        {
            if (LogProgress != null)
            {
                LogProgress.Report(_progressString);
                if ( (WorkingProgress != null) && (_progressString.EndsWith("...")) )
                {
                    WorkingProgress.Report(_progressString);
                }
            }
        }
    }
}
