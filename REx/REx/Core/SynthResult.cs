﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx.Core
{
    /// <summary>
    /// Class to represent a Graph as a result with the corresponding Tupels
    /// </summary>
    class SynthResult : IEquatable<SynthResult>
    {
        /// <summary>
        /// Graph from which the result is created
        /// </summary>
        public Graph ResultGraph
        {
            get; private set;
        }

        /// <summary>
        /// List of stencils for this result
        /// </summary>
        public List<Path> Stencil
        {
            get; private set;
        }

        /// <summary>
        /// Index of the Stencil form which the ResultTupel is created
        /// </summary>
        public int StencilIndex
        {
            get; private set;
        }

        /// <summary>
        /// Result Tupel (contains the Nodes)
        /// </summary>
        public List<List<Node>> ResultTuples
        {
            get { return getResultTuples(); }
            private set { }
        }

        /// <summary>
        /// Boolean to save, to markup a result which have to be removed
        /// </summary>
        public bool marked
        {
            get; set;
        }

        /// <summary>
        /// Create a SynthResult with the gevin parameters
        /// </summary>
        /// <param name="_resultGraph"></param>
        /// <param name="_stencil"></param>
        /// <param name="_stencilIndex"></param>
        /// <param name="_marked"></param>
        public SynthResult(Graph _resultGraph, List<Path> _stencil, int _stencilIndex, bool _marked = false)
        {
            ResultGraph = _resultGraph;
            Stencil = _stencil;
            StencilIndex = _stencilIndex;
            marked = _marked;
        }
        
        /// <summary>
        /// Get a List ov Nodes which represents the resulting Tupels of the current graph
        /// </summary>
        /// <returns></returns>
        private List<List<Node>> getResultTuples()
        {
            List<List<Node>> res = new List<List<Node>>();
            // fill each tuple index with corresponding nodes according to stencil
            Node graphRoot = ResultGraph.getAllNodes()[0];
            List<List<Node>> pathNodeSets = new List<List<Node>> { new List<Node> { graphRoot } };
            foreach (Path stencilPath in Stencil)
            {
                // store current traversal as List of Tuples that contain Path to Node and Node; start with root
                List<Tuple<Path, Node>> curTraversals = new List<Tuple<Path, Node>> { new Tuple<Path, Node>(new Path(), graphRoot) };
                // foreach edge of path
                foreach (Edge stencilPathEdge in stencilPath.Edges)
                {
                    List<Tuple<Path, Node>> nextTraversals = new List<Tuple<Path, Node>>();
                    List<string> extensionIds = new List<string>();
                    // go over each Node in previous traversal step
                    foreach (Tuple<Path, Node> curTraversal in curTraversals)
                    {
                        // check all outgoing edges
                        foreach (Edge outEdge in ResultGraph.getOutEdges(curTraversal.Item2))
                        {
                            // if outgoing edge matches edge in stencil at the current traversal depth
                            if (outEdge.Id == stencilPathEdge.Id)
                            {
                                // add path and node to next traversal step
                                List<Edge> nextEdges = curTraversal.Item1.Edges.ToList();
                                nextEdges.Add(outEdge);
                                nextTraversals.Add(new Tuple<Path, Node>(new Path(nextEdges), outEdge.Destination));
                            }
                        }
                    }
                    // set next traversal nodes
                    curTraversals = nextTraversals.ToList();
                }
                // after current stencil path is completed, all remaining traversals are valid according to stencil
                List<Node> curTupleElement = new List<Node>();
                foreach (Tuple<Path, Node> resTraversal in curTraversals)
                {
                    // add to results
                    if (!curTupleElement.Contains(resTraversal.Item2))
                    {
                        curTupleElement.Add(resTraversal.Item2);
                    }
                }
                pathNodeSets.Add(curTupleElement);
            }
            // create all possible combinations
            res = GraphUtils.getAllCombinations<Node>(pathNodeSets);
            return res;
        }

        /// <summary>
        /// Eqauls-comparison function
        /// </summary>
        /// <param name="_otherSynthResult"></param>
        /// <returns></returns>
        public bool Equals(SynthResult _otherSynthResult)
        {
            if (ResultGraph == _otherSynthResult.ResultGraph)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}