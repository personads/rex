﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx.Core
{
    class SynthEngine
    {
        public static string synthQuery(List<Path> _stencil)
        {
            string res = "";
            API.WikiDataQueryFilter filter = new API.WikiDataQueryFilter();
            foreach (Path path in _stencil)
            {
                filter.addSelector(path);
            }
            res = filter.synthFullQuery();
            return res;
        }

        /// <summary>
        /// Get all Stencils which connects togehter all graphs in the list.
        /// </summary>
        /// <param name="graphList">List of Graphs for whiche the Stencils are searched. The first Node of each Graph is the root node.</param>
        /// <returns></returns>
        public static List<List<Path>> findStencils(List<Graph> graphList)
        {
            List<List<Path>> stencils = new List<List<Path>>();
            // store root nodes of base graphs
            List <Node> rootNodes = new List<Node>();
            foreach (Graph g in graphList)
            {
                List<Node> nodes = g.getAllNodes();
                if (nodes.Count > 0)
                {
                    rootNodes.Add((g.getAllNodes())[0]);
                }
            }
            // join base graphs
            Graph graph = GraphUtils.joinGraphs(graphList);

            // start from all root nodes
            foreach (Node rootNode in rootNodes)
            {
                List<Node> searchNodes = new List<Node>(rootNodes);
                searchNodes.Remove(rootNode);
                bool isConnected = true;
                // store paths to search nodes
                Dictionary<Node, List<Path>> possiblePaths = new Dictionary<Node, List<Path>>();
                // search for path to each other root node (search node)
                foreach (Node searchNode in searchNodes)
                {
                    List<Path> paths = findPaths(graph, rootNode, searchNode);
                    // if paths to search node exist
                    if (paths.Count > 0)
                    {
                        // add to list of possible paths
                        if (possiblePaths.ContainsKey(searchNode))
                        {
                            possiblePaths[searchNode] = possiblePaths[searchNode].Concat(paths).ToList();
                        }
                        else
                        {
                            possiblePaths.Add(searchNode, paths);
                        }
                    } else
                    {
                        isConnected = false;
                        break;
                    }
                }
                if (isConnected)
                {
                    stencils = stencils.Concat(GraphUtils.getPathCombinations(possiblePaths)).ToList();
                }
            }
            return stencils;
        }

        /// <summary>
        /// Apply a Stencils on a Graph.
        /// </summary>
        /// <param name="_graph">Graph on which the Stencils will be apllied</param>
        /// <param name="_stencil"></param>
        /// <returns></returns>
        public static async Task<Graph> applyStencil(Graph _graph, List<Path> _stencil)
        {
            Graph res = null;
            List<Node> resNodes = new List<Node>();
            List<Edge> resEdges = new List<Edge>();
            Node graphRoot = _graph.getAllNodes()[0];
            resNodes.Add(graphRoot);
            API.WikiData wd = new API.WikiData();
            // traverse all paths
            foreach (Path stencilPath in _stencil)
            {
                // store current traversal as List of Tuples that contain Path to Node and Node; start with root
                List<Tuple<Path, Node>> curTraversals = new List<Tuple<Path, Node>> { new Tuple<Path, Node>(new Path(), graphRoot) };
                // foreach edge of path
                foreach (Edge stencilPathEdge in stencilPath.Edges)
                {
                    List<Tuple<Path, Node>> nextTraversals = new List<Tuple<Path, Node>>();
                    List<string> extensionIds = new List<string>();
                    // go over each Node in previous traversal step
                    foreach (Tuple<Path, Node> curTraversal in curTraversals)
                    {
                        // check all outgoing edges
                        foreach (Edge outEdge in _graph.getOutEdges(curTraversal.Item2))
                        {
                            // if outgoing edge matches edge in stencil at the current traversal depth
                            if (outEdge.Id == stencilPathEdge.Id)
                            {
                                // add path and node to next traversal step
                                List<Edge> nextEdges = curTraversal.Item1.Edges.ToList();
                                nextEdges.Add(outEdge);
                                nextTraversals.Add(new Tuple<Path, Node>(new Path(nextEdges), outEdge.Destination));
                                // extend graph if necessary
                                if (_graph.getOutEdges(outEdge.Destination).Count < 1)
                                {
                                    extensionIds.Add("Q" + outEdge.Destination.Id);
                                }
                            }
                        }
                    }
                    // perform extension operation
                    if (extensionIds.Count > 0)
                    {
                        List<Graph> extensionGraphs = await wd.getEntities(extensionIds);
                        extensionGraphs.Insert(0, _graph);
                        _graph = GraphUtils.joinGraphs(extensionGraphs);
                    }
                    // set next traversal nodes
                    curTraversals = nextTraversals.ToList();
                }
                // after current stencil path is completed, all remaining traversals are valid according to stencil
                foreach (Tuple<Path, Node> resTraversal in curTraversals)
                {
                    // add to results
                    resNodes.Add(resTraversal.Item2);
                    foreach (Edge resTraversalEdge in resTraversal.Item1.Edges)
                    {
                        resEdges.Add(resTraversalEdge);
                    }
                }
            }
            // construct graph
            if ((resNodes.Count > 0) && (resEdges.Count > 0))
            {
                res = new Graph(resNodes, resEdges);
            }
            return res;
        }

        /// <summary>
        /// Finds all paths in a Graph from on starting node to a destination node
        /// </summary>
        /// <param name="_graph"></param>
        /// <param name="_source"></param>
        /// <param name="_destination"></param>
        /// <param name="_visited"></param>
        /// <returns></returns>
        public static List<Path> findPaths(Graph _graph, Node _source, Node _destination, List<Node> _visited = null)
        {
            List<Path> res = new List<Path>();
            _visited = _visited ?? new List<Node>();
            if (!_visited.Contains(_source))
            {
                _visited.Add(_source);
                foreach (Edge outEdge in _graph.getOutEdges(_source))
                {
                    if (outEdge.Destination.Equals(_destination))
                    {
                        res.Add(new Path (new List<Edge> { outEdge }));
                    }
                    // searches recursively
                    else {
                        foreach (Path path in findPaths(_graph, outEdge.Destination, _destination, _visited))
                        {
                            path.insertEdge(0, outEdge);
                            res.Add(path);
                        }
                    }
                }
                _visited.Remove(_source);
            }
            return res;
        }
        
        /// <summary>
        /// Extends a Graph by a given depth. This means, that every node of a outgoing edge will be added to the graph and the edges of this nodes will also be added to the Graph.
        /// </summary>
        /// <param name="_graph"></param>
        /// <param name="_depth"></param>
        /// <returns></returns>
        public static async Task<Graph> extendGraph(Graph _graph, int _depth)
        {
            Graph res = _graph;
            API.WikiData wd = new API.WikiData();
            for (int i = 0; i < _depth; i++)
            {
                HashSet<string> terminalNodeIds = new HashSet<string>();
                foreach (Node terminalNode in _graph.getTerminalNodes())
                {
                    terminalNodeIds.Add("Q" + terminalNode.Id);
                }
                List<Graph> extensionGraphs = await wd.getEntities(terminalNodeIds.ToList());
                extensionGraphs.Insert(0, res);
                res = GraphUtils.joinGraphs(extensionGraphs);
            }
            return res;
        }

        /// <summary>
        /// Gets all Labels for every Node of the Graph
        /// </summary>
        /// <param name="_graph"></param>
        /// <returns></returns>
        public static async Task<Graph> labelGraph(Graph _graph)
        {
            Graph res = _graph;
            API.WikiData wd = new API.WikiData();
            HashSet<string> unlabelledNodeIds = new HashSet<string>();
            foreach (Node node in _graph.getAllNodes())
            {
                if (node.Label == "")
                {
                    unlabelledNodeIds.Add("Q" + node.Id);
                }
            }
            // TODO : labels for property edges
            List<Graph> extensionGraphs = await wd.getEntities(unlabelledNodeIds.ToList());
            foreach (Graph extensionGraph in extensionGraphs)
            {
                res.addNode(extensionGraph.getAllNodes()[0]);
            }
            return res;
        }
    }
}
