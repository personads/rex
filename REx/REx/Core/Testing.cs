﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace REx.Core
{
    class Testing
    {
        public static void joinGraphsTest()
        {
            Node node1 = new Node(1, "erster Knoten");
            Node node2 = new Node(2, "zweiter Knoten");
            Node node3 = new Node(3, "dritter Knoten");
            Node node4 = new Node(4, "virter Knoten");

            Edge edge1 = new Edge(201, node1, node2, "Kante zwischen 1 und 2");
            Edge edge2 = new Edge(202, node3, node4, "Kante zwischen 3 und 4");
            Edge edge3 = new Edge(444, node2, node3, "Kannte zwischen 2 und 3");
            Edge emptyEdge = new Edge(999);

            Graph g1 = new Graph(node1);
            g1.addEdge(edge1);
            Graph g2 = new Graph(node2);
            g2.addEdge(edge2);
            Graph g3 = new Graph(node3);
            g3.addEdge(edge3);
            Graph g4 = new Graph(node4);
            g4.addEdge(emptyEdge);

            List<Graph> graphliste = new List<Graph>();
            graphliste.Add(g1);
            graphliste.Add(g4);
            graphliste.Add(g3);
            graphliste.Add(g2);

            Graph bigJoinGraph = GraphUtils.joinGraphs(graphliste);

            Console.WriteLine("Nodes of bigJoinGraph");
            foreach (Node n in bigJoinGraph.getAllNodes())
            {
                Console.WriteLine("Id:" + n.Id + ":" + n.Label);
            }
            Console.WriteLine("Edges of bigJoinGrap");
            foreach (Edge e in bigJoinGraph.getAllEdges())
            {
                Console.WriteLine("Id:" + e.Id + ":" + e.Label);
            }
        }

        public static async void wikidataqueryTest()
        {
            Console.WriteLine("Testing WikiDataQuery API...");
            API.WikiDataQuery wdq = new API.WikiDataQuery("https://wdq.wmflabs.org/api");
            Console.WriteLine("- wdqentities");
            foreach (int id in await wdq.getEntities("claim[170:762]"))
            {
                Console.WriteLine(id);
            }
        }

        public static async void wikidataTest()
        {
            Console.WriteLine("Testing WikiData API...");
            API.WikiData wd = new API.WikiData("https://www.wikidata.org/w/api.php", "en");
            Console.WriteLine("- wbgetentities");
            foreach (Graph entity in await wd.getEntities(new List<string> { "Q12418", "Q42" }))
            {
                Console.WriteLine(entity.getAllNodes()[0].Id.ToString() + ":" + entity.getAllNodes()[0].Label);
                foreach (Edge property in entity.getAllEdges())
                {
                    Console.WriteLine(property.Source.Id.ToString() + " -P" + property.Id.ToString() + "-> " + property.Destination.Id.ToString());
                }
            }
            Console.WriteLine("- wbsearchentities");
            foreach (Node entity in await wd.searchEntities("Mona Lisa"))
            {
                Console.WriteLine(entity.Id.ToString() + ":" + entity.Label);
            }
        }

        public static void joinGraphTest()
        {
            Node node1 = new Node(1, "erster Knoten");
            Node node2 = new Node(2, "zweiter Knoten");
            Node node3 = new Node(3, "dritter Knoten");
            Node node4 = new Node(4, "virter Knoten");
            Graph graph = new Graph(node1);
            graph.addNode(node2);
            Edge edge1 = new Edge(201, node1, node2, "Kante zwischen 1 und 2");
            graph.addEdge(edge1);
            Edge edge2 = new Edge(202, node3, node4, "Kante zwischen 3 und 4");
            Graph graph2 = new Graph(node3);
            graph2.addEdge(edge2);
            Edge edge3 = new Edge(444, node2, node3, "Kannte zwischen 2 und 3");
            graph.addEdge(edge3);
            graph.joinGraph(graph2);
            Console.WriteLine("Nodes after graph.joinGraph(graph2)");
            foreach (Node n in graph.getAllNodes())
            {
                Console.WriteLine("Id:" + n.Id + ":" + n.Label);
            }
            Console.WriteLine("Edges after graph.joinGraph(graph2)");
            foreach (Edge e in graph.getAllEdges())
            {
                Console.WriteLine("Id:" + e.Id + ":" + e.Label);
            }

            //join mit sich selbst Testen
            graph.joinGraph(graph);
            Console.WriteLine("Nodes after graph.joinGraph(graph)");
            foreach (Node n in graph.getAllNodes())
            {
                Console.WriteLine("Id:" + n.Id + ":" + n.Label);
            }
            Console.WriteLine("Edges after graph.joinGraph(graph)");
            foreach (Edge e in graph.getAllEdges())
            {
                Console.WriteLine("Id:" + e.Id + ":" + e.Label);
            }

            Graph miniGraph = new Graph(new Node(87, "MiniKnoten"));
            graph.joinGraph(miniGraph);
            Console.WriteLine("Nodes after graph.joinGraph(miniGraph);");
            foreach (Node n in graph.getAllNodes())
            {
                Console.WriteLine("Id:" + n.Id + ":" + n.Label);
            }
            Console.WriteLine("Edges after graph.joinGraph(miniGraph);");
            foreach (Edge e in graph.getAllEdges())
            {
                Console.WriteLine("Id:" + e.Id + ":" + e.Label);
            }
        }
    }
}
