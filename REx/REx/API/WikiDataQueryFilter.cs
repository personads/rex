﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REx.API
{
    /// <summary>
    /// Class to filter the wikidataquery by given several paths
    /// </summary>
    class WikiDataQueryFilter
    {
        private List<Path> selectors;
        private List<Path> exclusions;

        /// <summary>
        /// Default constructor
        /// </summary>
        public WikiDataQueryFilter()
        {
            this.selectors = new List<Path>();
            this.exclusions = new List<Path>();
        }

        /// <summary>
        /// Get all Selectors for the filter
        /// </summary>
        /// <returns>Paths which represents the selectors</returns>
        public List<Path> getSelectors()
        {
            return this.selectors;
        }

        /// <summary>
        /// Add a path to the filter selector
        /// </summary>
        /// <param name="_selector">Path which will be added to the filter</param>
        public void addSelector(Path _selector)
        {
            if (!this.hasSelector(_selector))
            {
                this.selectors.Add(_selector);
                minimizeSelectors();
            }
        }

        /// <summary>
        /// Checks if a Path is already in the list of selectors
        /// </summary>
        /// <param name="_selector"></param>
        /// <returns></returns>
        public bool hasSelector(Path _selector)
        {
            return selectors.Contains(_selector);
        }

        /// <summary>
        /// Function which shrinks all selectors of the filter. It removes already covered paths.
        /// E.g.: 1->2 will be removed if it is a subpath of a other pate, e.g. 1->2->3
        /// </summary>
        private void minimizeSelectors()
        {
            List<Path> minimizedSelectors = new List<Path>(selectors);
            foreach (Path selector in selectors)
            {
                Path longestMatch = selector;
                foreach (Path compSelector in minimizedSelectors)
                {
                    bool isMatch = true;
                    for (int i = 0; i < compSelector.Edges.Count; i++)
                    {
                        if (i < longestMatch.Edges.Count)
                        {
                            if (!longestMatch.Edges[i].Equals(compSelector.Edges[i]))
                            {
                                isMatch = false;
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    if ((isMatch) && (compSelector.Edges.Count > longestMatch.Edges.Count))
                    {
                        longestMatch = compSelector;
                    }
                }
                if (!longestMatch.Equals(selector))
                {
                    minimizedSelectors.Remove(selector);
                }
            }
            selectors = minimizedSelectors.ToList();
        }

        // TODO: remove?
        public void intersect(WikiDataQueryFilter _wdqfilter)
        {
            List<Path> intersectSelectors = new List<Path>();
            foreach (Path intersectSelector in _wdqfilter.getSelectors())
            {
                if (this.hasSelector(intersectSelector))
                {
                    intersectSelectors.Add(intersectSelector);
                }
            }
            this.selectors = intersectSelectors;
        }

        // TODO: remove?
        public void union(WikiDataQueryFilter _wdqfilter)
        {
            foreach (Path selector in _wdqfilter.getSelectors())
            {
                this.addSelector(selector);
            }
        }

        /// <summary>
        /// Create a query for wikidataquery by a selectro (Path)
        /// </summary>
        /// <param name="_selector">selector for wikidataquery</param>
        /// <returns></returns>
        public string synthSelectorQuery(Path _selector)
        {
            string res = "";
            if (_selector.Edges.Count > 0)
            {
                foreach (Edge edge in _selector.Edges)
                {
                    res += "claim[" + edge.Id + ":(";
                }
                res = res.Substring(0, res.Length - 2);
                for (int i = 0; i < _selector.Edges.Count - 1; i++)
                {
                    res += "])";
                }
                res += "]";
            }
            return res;
        }

        /// <summary>
        /// Create a full query for wikidataquery which covers all selectros from the filter
        /// </summary>
        /// <returns></returns>
        public string synthFullQuery()
        {
            string res = "";
            if (this.selectors.Count > 0)
            {
                foreach (Path selector in this.selectors)
                {
                    res += this.synthSelectorQuery(selector) + " AND ";
                }
                res = res.Substring(0, res.Length - 5);
            }
            return res;
        }

        /// <summary>
        /// Return the full query for wikidataquery as a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.synthFullQuery();
        }
    }
}
