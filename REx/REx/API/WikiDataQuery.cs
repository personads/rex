﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace REx.API
{
    /// <summary>
    /// Class for Wikidataquery
    /// </summary>
    class WikiDataQuery
    {
        private string baseurl;

        /// <summary>
        /// Constructor for Wikidataquery
        /// </summary>
        /// <param name="_baseurl">URL of the web api from wikidataquery</param>
        public WikiDataQuery(string _baseurl = "https://wdq.wmflabs.org/api")
        {
            this.baseurl = _baseurl;
        }

        /// <summary>
        /// A asynchron task to get all Entites by a given query from wikidata
        /// </summary>
        /// <param name="_query">Queryparameter for wikidataquery</param>
        /// <returns>Return a list of ids of the Entities which was returned by the wikidataquery (these who match to the given query)</returns>
        public async Task<List<int>> getEntities(string _query)
        {
            List<int> res = new List<int>();
            Dictionary<string, string> args = new Dictionary<string, string> { { "q", _query } };
            Query query = new Query(this.baseurl, args);
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(WikiDataQueryResponse));
            MemoryStream responseStream = new MemoryStream(Encoding.UTF8.GetBytes(await query.getResponseAsString() ?? ""));
            WikiDataQueryResponse response = (WikiDataQueryResponse)serializer.ReadObject(responseStream);
            res = response.items;
            return res;
        }

        // TODO: was kann man hier kommentieren?
        [DataContract]
        private class WikiDataQueryResponse
        {
            [DataMember(Name = "status")]
            public WikiDataQueryResponseStatus status { get; protected set; }

            [DataMember(Name = "items", IsRequired = true)]
            public List<int> items { get; protected set; }
        }
        // TODO: siehe oben
        // TODO: werden die ganzen member der classe überhaupt gebraucht? wird überhaupt die ganze klasse gebraucht?
        [DataContract]
        private class WikiDataQueryResponseStatus
        {
            [DataMember]
            public string error { get; protected set; }

            [DataMember]
            public int items { get; protected set; }

            [DataMember]
            public string querytime { get; protected set; }

            [DataMember(Name = "parsed_query")]
            public string parsedQuery { get; protected set; }
        }
    }
}
