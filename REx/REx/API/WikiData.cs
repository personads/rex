﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace REx.API
{
    /// <summary>
    /// Class for calls at the Wikidata web api
    /// </summary>
    class WikiData
    {
        private string baseurl;
        private string lang;

        /// <summary>
        /// Create a Wikidata object
        /// </summary>
        /// <param name="_baseurl">Url from the web api</param>
        /// <param name="_lang">(otional) language parameter (default is "en")</param>
        public WikiData(string _baseurl = "https://www.wikidata.org/w/api.php", string _lang = "en")
        {
            this.baseurl = _baseurl;
            this.lang = _lang;
        }

        /// <summary>
        /// Create a Dictionary with Key-Value: "format", "xml"
        /// </summary>
        /// <returns></returns>
        private Dictionary<string,string> genBaseArguments()
        {
            return new Dictionary<string, string> { { "format", "xml" } };
        }

        /// <summary>
        /// A asynchron Task to get all Entities for some given ids from Wikidata
        /// </summary>
        /// <param name="_ids">Ids of the requested Entities</param>
        /// <returns></returns>
        public async Task<List<Graph>> getEntities(List<string> _ids)
        {
            List<Graph> res = new List<Graph>();
            if (_ids.Count < 1)
            {
                return res;
            }
            else if (_ids.Count > 50)
            {
                List<string> idsPartition = new List<string>();
                for ( int i = 0; i < _ids.Count; i++)
                {
                    idsPartition.Add(_ids[i]);
                    if (i%30 == 0)
                    {
                        res = res.Concat(await getEntities(idsPartition)).ToList();
                        idsPartition.Clear();
                    }
                }
                if (idsPartition.Count > 0)
                {
                    res = res.Concat(await getEntities(idsPartition)).ToList();
                }
            }
            else {
                Dictionary<string, string> args = this.genBaseArguments();
                args.Add("action", "wbgetentities");
                args.Add("ids", "");
                foreach (string id in _ids)
                {
                    args["ids"] += id + "|";
                }
                args["ids"] = args["ids"].Substring(0, args["ids"].Length - 1);
                args.Add("languages", this.lang);
                Query query = new Query(this.baseurl, args);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(await query.getResponseAsString());
                foreach (XmlNode xmlEntity in xml.SelectNodes("/api/entities/entity"))
                {
                    int curId = Int32.Parse(xmlEntity.Attributes["id"].InnerText.Substring(1));
                    string curLabel = "";
                    if ((xmlEntity["labels"] != null) && (xmlEntity["labels"]["label"] != null))
                    {
                        curLabel = xmlEntity["labels"]["label"].GetAttribute("value");
                    }
                    Node curEntityNode = new Node(curId, curLabel);
                    Graph curGraph = new Graph(curEntityNode); // start graph with entity as root node
                    foreach (XmlNode xmlProperty in xmlEntity.SelectNodes("claims/property"))
                    {
                        int curPropertyId = Int32.Parse(xmlProperty.Attributes["id"].InnerText.Substring(1));
                        foreach (XmlNode xmlPropertyClaim in xmlProperty.SelectNodes("claim"))
                        {
                            XmlElement curXmlDataValue = xmlPropertyClaim["mainsnak"]["datavalue"];
                            if (curXmlDataValue != null && curXmlDataValue.GetAttribute("type") == "wikibase-entityid")
                            {
                                int curPropertyNodeId = Int32.Parse(curXmlDataValue["value"].GetAttribute("numeric-id"));
                                Node curPropertyNode = new Node(curPropertyNodeId);
                                curGraph.addEdge(new Edge(curPropertyId, curEntityNode, curPropertyNode));
                            }
                        }
                    }
                    res.Add(curGraph);
                }
            }
            return res;
        }

        /// <summary>
        /// A asynchron task to searche wikidata after entities by given search key (string)
        /// </summary>
        /// <param name="_query">The searchparameter</param>
        /// <param name="_limit">Limit for the output from the search results.
        /// Default = 10</param>
        /// <param name="_continue"></param>
        /// <returns>Returns a list of nodes which represents the search results</returns>
        public async Task<List<Node>> searchEntities(string _query, int _limit=10, int _continue=0)
        {
            List<Node> res = new List<Node>();
            Dictionary<string, string> args = this.genBaseArguments();
            args.Add("action", "wbsearchentities");
            args.Add("search", _query);
            args.Add("language", this.lang);
            args.Add("limit", _limit.ToString());
            args.Add("continue", _continue.ToString());
            Query query = new Query(this.baseurl, args);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(await query.getResponseAsString());
            foreach (XmlNode xmlEntity in xml.SelectNodes("/api/search/entity"))
            {
                int curId = Int32.Parse(xmlEntity.Attributes["id"].InnerText.Substring(1));
                string curLabel = xmlEntity.Attributes["label"].InnerText;
                res.Add(new Node(curId, curLabel));
            }
            return res;
        }
    }
}
