﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;

namespace REx.API
{
    /// <summary>
    /// Class to call queries on a given url
    /// </summary>
    class Query
    {
        private string url;
        private Dictionary<string,string> parameters;

        /// <summary>
        /// Create a query
        /// </summary>
        /// <param name="_url">URL of the website/applikation where the query have to run</param>
        /// <param name="_parameters">Parameters for the query</param>
        public Query(string _url, Dictionary<string,string> _parameters)
        {
            this.url = _url;
            this.parameters = _parameters;
        }

        /// <summary>
        /// An asynchrone task to execute a query request (via http)
        /// </summary>
        /// <returns>Return the http response as a string</returns>
        public async Task<string> execute()
        {
            string res;
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(this.parameters);
                var response = await client.PostAsync(this.url, content);
                response.EnsureSuccessStatusCode();
                res = await response.Content.ReadAsStringAsync();
            }
            return res;
        }

        /// <summary>
        /// An asynchrone task to get the response from a query request
        /// </summary>
        /// <returns>Returns the HTTP response from the query</returns>
        public async Task<string> getResponseAsString()
        {
            return await execute();
        }

        // TODO: remove?
        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
