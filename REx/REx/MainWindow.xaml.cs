﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace REx
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool isWorking = false;
        List<Core.SynthResult> synthResults;
        Core.SynthTask curSynthTask;

        Progress<string> workingProgress;
        Progress<string> logProgress;

        public MainWindow()
        {
            InitializeComponent();
            button.IsEnabled = false;
        }

        /// <summary>
        /// Function which start the whole synth process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void txtInput_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (!isWorking)
                {
                    toggleWorking();
                    appendLog("===== SynthTask =====");
                    workingProgress = new Progress<string>(changeProgressText);
                    logProgress = new Progress<string>(appendLog);
                    string[] inputTerms = txtInput.Text.Split('|');
                    curSynthTask = new Core.SynthTask(inputTerms, null, null, null, 1, workingProgress, logProgress);
                    synthResults = await curSynthTask.getSynthResults();
                    appendLog("\n===== End SynthTask =====\n");
                    showResults(synthResults);
                    toggleWorking();
                    button.IsEnabled = true;
                }
            }
        }

        private void txtLog_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtLog.ScrollToEnd();
        }

        private void appendLog(string _logText)
        {
            txtLog.AppendText(_logText + "\n");
        }

        private void changeProgressText(string _progress)
        {
            txtProgress.Text = _progress;
        }

        // TODO: an neue SynthResult klasse anpassen
        private void showResults(List<Core.SynthResult> _synthResult)
        {
            lbResults.ItemsSource = _synthResult;
        }

        private void toggleWorking()
        {
            if (isWorking)
            {
                txtProgress.Visibility = Visibility.Hidden;
                tabResultsGrid.IsEnabled = true;
                isWorking = false;
            }
            else
            {
                txtProgress.Visibility = Visibility.Visible;
                tabResultsGrid.IsEnabled = false;
                isWorking = true;
            }
        }

        /// <summary>
        /// Event to markup Results which have to be removed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListView;
            var context = item.DataContext;
            if (context != null)
            {
                if (context.GetType() == typeof(REx.Core.SynthResult))
                {
                    if((context as REx.Core.SynthResult).marked == false)
                    {
                        (context as REx.Core.SynthResult).marked = true;
                        item.Background = Brushes.Gray;
                    }
                    else
                    {
                        (context as REx.Core.SynthResult).marked = false;
                        item.Background = Brushes.White;
                    }
                }
            }
        }

        /// <summary>
        /// Remove marked results and the corresponding stencils
        /// Start new synth process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Click(object sender, RoutedEventArgs e)
        {
            if (!isWorking)
            {
                toggleWorking();
                button.IsEnabled = false;
                appendLog("===== Remove wrong stencils =====");
                var markedResults = synthResults.FindAll(x => x.marked == true);
                if (markedResults.Count != 0)
                {
                    var stencils = curSynthTask.Stencils;
                    int i = 0;
                    foreach (var result in markedResults)
                    {
                        if(stencils.Remove(result.Stencil))
                        {
                            i++;
                        }
                    }
                    appendLog("\n" + i + " stencils where removed");
                    curSynthTask.Stencils = stencils;
                    curSynthTask.ResultGraphSets = new List<List<Graph>>();
                    appendLog("\n===== End remove wrong stencils =====\n");
                    appendLog("===== SynthTask =====");
                    synthResults = await curSynthTask.getSynthResults();
                    appendLog("\n===== End SynthTask =====\n");
                    showResults(synthResults);
                }
                toggleWorking();
                button.IsEnabled = true;
            }
        }
    }
}
