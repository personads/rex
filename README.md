# ReX
A relational data extractor for [WikiData](https://www.wikidata.org/) which iteratively synthesizes extraction queries given examples.

Provided the example 'Mona Lisa', 'Leonardo da Vinci' and 'oil paint' ReX would for instance attempt to synthesize queries to extract graphs that represent works of art created by an artist using a certain material.
While this would be complicated to accomplish using manual API-calls, ReX uses program synthesis techniques to generate graphs in an example-driven way, automatically and efficiently. The provided GUI also enables users to do this without writing a single line of code.

## Requirements
A C#-compiler and standard packages provided in the .NET-framework. No further libraries are needed.  
Core packages can function without Windows UI libraries.

## Components

The ReX project contains the following components:

* **API**
	* **Query** is the superclass used to make standard API-calls via HTTP
	* **WikiData** is used to make specific calls to the WikiData API
	* **WikiDataQuery** is used to make specific calls to the WikiDataQuery API
	* **WikiDataQueryFilter** provides a C# implementation of WDQ selector filters which can be passed to the WDQ API
* **Core**
	* **SynthEngine** communicates with the APIs to synthesize relational data queries in an example-driven manner
	* **SynthResult** contains result Graphs with metadata used during synthesis
	* **SynthTask** asynchronous task wrapper for synthesis
	* **Testing** small class testing basic functionality of core components
* **Graph**
	* **Edge** class to store edge information such as source, destination, id and label
	* **Graph** class to store directed graphs and corresponding metadata
	* **GraphBinding** is used to more easily display graphs in XAML UI elements (e.g. ListView)
	* **Node** class to store graph nodes and corresponding metadata
	* **Path** class to store traversal paths through a graph
* **MainWindow** provides a simple GUI for interacting with the underlying framework

More information on the components can be found in both the documentation and the code docstrings themselves.

## Interface

![Screenshot of the ReX GUI](docs/Images/screenshot.png "Screenshot")

To make use of ReX through the GUI, simply input your example relation in the search bar at the top in the following format:

```
Item 0 | Item 1 | ... | Item n
```

Start the synthesis by pressing the `Return` key. ReX will then search for Items corresponding to your textual inputs and synthesize queries to retrieve possible graphs. These are returned in the Results Tab.

Double click a graph to mark it as invalid. This will be indicated by it greying out. The synthesis will then be run again to generate more plausible queries and result graphs.

The process can be follwed in the Log Tag where all API calls and synthesis processes are verbosely logged.

## Documentation
Functional descriptions and usage examples can be found in `docs/REx.pdf`. 

The project's progression can be followed through presentations in `pres/`.
Please refer to the final presentation for a detailed examination of the project.

---

We thank the [WikiData](https://www.wikidata.org/) project for their continuing efforts and hope you find the information you need.