\select@language {ngerman}
\contentsline {section}{\numberline {1}Motivation}{3}{section.1}
\contentsline {section}{\numberline {2}Grundlagen}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Graphen}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Wikidata Query}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}L\IeC {\"o}sungsansatz}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Programmierumgebung}{5}{section.3}
\contentsline {section}{\numberline {4}Architektur}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Graph}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}API}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Core}{7}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}UI}{7}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Sequenzdiagramm}{8}{subsection.4.5}
\contentsline {section}{\numberline {5}Verwendung}{10}{section.5}
\contentsline {section}{\numberline {6}Weitere Verbesserungen}{11}{section.6}
\contentsline {section}{\numberline {7}Fazit}{11}{section.7}
