% KOMA-Script für mittlere Schriftwerke (z.B.: Semesterarbeiten)
\documentclass[12pt]{scrartcl}

% Seitenränder und Abstände
\usepackage{geometry}
\geometry{a4paper, top=30mm, left=30mm, right=30mm, bottom=30mm, headsep=10mm, footskip=7mm}

% Zeichenkodierung
\usepackage[utf8]{inputenc}

% neue deutsche Rechtschreibung, insbesondere für Silbentrennung
\usepackage[ngerman]{babel}

% kodierung für Westeuropäische Zeichen
\usepackage[T1]{fontenc}

% bessere Schrifftdarstellung für PDFs
\usepackage{lmodern}

% Curier schriftart
\usepackage{courier}

% 1,5 facher Zeilenabstand
\usepackage{setspace}

% Packet um Blindtext zu generieren (um Design zu Testen)
\usepackage{blindtext}

% Tabbelenbeschriftung linksbündig
\usepackage[justification=RaggedRight, singlelinecheck=false]{caption} 

% Textumschließende Tabellen ermöglichen
%\usepackage{wrapfig}

% URLs
\usepackage{hyperref}

\usepackage{graphicx}

% für Tabellen mit automatischem Zeilenumbruch
\usepackage{tabularx}

\begin{document}
	\include{Titlepage}
	% Titelseite
	\maketitle
	\thispagestyle{empty}
	\newpage
	% 1.5 facher Zeilenabstand
	\onehalfspacing	
	% Inhaltsverzeichnis
	\tableofcontents
	\newpage
	
	\section{Motivation}
Im Rahmen des Softwarepraktikums {\lq}Programm Synthese f\"ur Fortgeschrittene{\rq} wurde eine L\"osung zum Problem der Extraktion relationaler Daten angestrebt.\\
Hierbei sollte durch die alleinige Angabe eines Beispiel n-Tupels und dem iterativen Markieren falscher Ergebnisse eine Ansammlung an neuen Tupeln generiert werden, zwischen deren jeweiligen Elementen korrespondierende Relationen bestehen. Diese simple Vorgehensweise erm\"oglicht es selbst Endnutzern ohne tiefgehende technische Kenntnisse bez\"uglich Anfragen auf semi-strukturierte Datenbanken, komplexe Zusammenh\"ange darzustellen und neue Daten zu gewinnen, ohne eine einzige Zeile SQL, SPARQL o.\"A. zu schreiben.\\
Unsere hierin vorgestellte L\"osung zu dieser Problemstellung nennt sich \textbf{R}elational Data \textbf{Ex}tractor oder kurz \textbf{REx}.

	\section{Grundlagen}
	\subsection{Wikidata}
Die Wahl der semi-strukturierten Datenquelle f\"ur dieses Projekt fiel auf das Wikidata Projekt der Wikimedia Foundation\footnote{\url{https://www.wikidata.org/}}.\\
Uns \"uberzeugte hierbei die Vielfalt, sowie die feingranulare Aufschl\"usselung der Daten und der gut geregelte Zugriff auf diese \"uber eine Wikimedia Standard-API.\\
Die semi-strukturierten Daten in Wikidata liegen als Graphen vor, deren Knoten mit Artikeln oder Entit\"aten aus der Wikipedia vergleichbar sind und deren Beziehungen untereinander die dazwischen liegenden Kanten bilden. Die Wikipedia Artikel (Items) bilden die Knoten im Graphen und haben alphanumerische IDs beginnend mit Q. Jedes Item hat mehrere Eigenschaften (Properties), deren IDs mit P beginnen. Die Eigenschaften repräsentieren die Kanten eines Graphen und somit die Beziehung zwischen zwei Knoten. In Wikidata ist jede Beziehung nur in eine Richtung gültig und somit entstehen immer gerichtete Graphen. Ein einfaches Beispiel bilden die zwei Items „Douglas Adams“ und „The Hitchhiker's Guide to the Galaxy pentalogy“ und ihre Beziehung „notable work“ (ausgehend von „Douglas Adams“), die als nachfolgender Graph interpretiert werden k\"onnen:\\
\begin{center}
	\includegraphics[width=1\textwidth]{Images/Douglas_Adams.pdf}
\end{center}

	\subsection{Wikidata Query}
Aus den obigen Graphen lassen sich bereits die notwendigen Relationen extrahieren, welche das eingegebene n-Tupel ausmachen. Sobald diese Informationen aus der Eingabe und dem resultierenden Graphen extrahiert sind, muss eine komplexere Anfrage an Wikidata gestellt werden, um korrespondierende Tupel zu finden. Allerdings sind auf die Wikidata-API keine Anfragen m\"oglich, denen Filter, wie z.B. ausgehende Kanten und somit Relationen, zugrunde liegen.\\
Bei der Recherche hat sich hierfür Wikidata Query \footnote{\url{https://wdq.wmflabs.org/}} als passendes Tool erwiesen. Bei Wikidata Query handelt es sich um eine in der Beta-Phase befindliche Online API der Wikimedia Tool Labs, welche basierend auf Filterangaben IDs passender Wikidata-Items zur\"uckgeben kann. Dabei arbeitet Wikidata Query auf einem Abbild („Dump“) Wikidatas. Dies hat zwar den Nachteil ggf. veralteter Daten, bietet jedoch den Vorteil der schnelleren Ausführungszeit. Zudem wurden in zahlreichen Testdurchl\"aufen keinerlei Probleme mit asynchronen Daten festgestellt.\\
Auf das Graphen Beispiel von oben angewendet, bedeutet dies, dass die Anfrage \textit{CLAIM[P800]}(„alle Knoten mit der Beziehung „notable work“) jeweils alle Items zurückgibt, die eine ausgehende Kante („notable work“) besitzen.\\
Für weitere Informationen für den Aufbau aller Anfragetypen siehe: \url{http://wdq.wmflabs.org/api_documentation.html}.\\
Die Aufgabe REx ist es nun basierend auf dem Eingabetupel und dem Feedback des Nutzers Filterangaben f\"ur Wikidata Query zu synthetisieren und die Ergebnisgraphen aufzubereiten.

	\subsection{Problemstellung und L\"osungsansatz}
Um die Extraktion automatisch zu ermöglichen sind drei Hauptschritte notwendig:
\begin{itemize}
\item Relationsextraktion aus Eingabetupel
\item Query-Synthese und Ergebnisanfrage
\item Entfernen fehlerhafter Ergebnisse
\end{itemize}

	\subsubsection*{Relationsextraktion aus Eingabetupel}
Um ein Eingabetupel richtig interpretieren zu k\"onnen, muss dieses zuerst in seine einzelnen Elemente zerlegt werden. Da diese Elemente zun\"achst reiner Freitext sind, wird anschließend für jedes Element eine Wikidata Suche gestartet, welche die passenden Treffer-Items zurückgibt.\\
Im n\"achsten Schritt werden die lokalen Graphen, bestehend aus den jeweiligen Treffer-Items und ihren \"uber eine einzelne Kante verbundenen Items, generiert.\\
Da es zu jedem Suchterm eine gro{\ss}e Anzahl an Treffern und somit auch lokale Graphen gibt, \"uberpr\"uft REx jede Graphenkombination. Diese Kombinationen ergeben sich aus dem Join aller lokalen Graphen in einen einzigen Tupel-Graphen.\\
Ausgehend von den Wurzelknoten der urspr\"unglichen lokalen Graphen wird nun nach Kanten gesucht, welche diese verbinden. Da es sich um gerichtete Graphen handelt, muss jedes Element des Eingabetupels als möglicher Startknoten des Graphen betrachtet werden. Die gefundenen Kanten bilden anschließend einen Teilgraphen, welcher die Pfade zwischen den Wurzelknoten enth\"alt.\\
Sollte kein zusammenhängender Graph entstehen, bedeutet dies, dass keine direkten Beziehungen erkannt werden konnten und der Algorithmus terminiert. \"Uber einen Funktionsparameter ist es allerdings m\"oglich, eine Erweiterungstiefe einzustellen. In diesem Falle werden die lokalen Graphen bis zur maximalen Tiefe erweitert (lokale Grapen werden an Terminalknoten angehangen) bis eine Verbindung gefunden wird.\\
Entsteht auf direktem oder erweitertem Wege nun ein zusammenhängender Graph, der alle Eingabeelemente als Knoten besitzt, so werden im nächsten Schritt die Pfade extrahiert, welche die Eingabeknoten miteinander verbinden.\\
Die Pfade müssen jeden Eingabeknoten mit allen anderen Eingabeknoten verbinden (transitive Verbindungen sind erlaubt). Es kann passieren, dass in einem Graphen mehrere unterschiedliche Pfade gefunden werden. Da nicht bekannt ist, welcher der „richtige“ ist, müssen alle weiteren gefundenen Pfade betrachtet werden.\\
Im nachfolgenden ein schematisches Beispiel, bei dem mehrere Pfade existieren:\\
\begin{center}
	\includegraphics[width=0.5\textwidth]{Images/Pfade.pdf}\\
\end{center}

Die zwei möglichen Pfade sind:\\
\begin{center}
\begin{tabular}{|c|p{4cm}|}
\hline 
Pfad & Kanten(-Reihenfolge) \\ 
\hline 
$A\rightarrow B\rightarrow C$ & 1, 2 \\ 
\hline 
$A\rightarrow B$, $A\rightarrow C$ &  1 \newline 2 \\ 
\hline 
\end{tabular}
\end{center}

Die validen Pfade und ihre Kanten bilden die Schablonen (Stencils) der weiteren Anfragen, mit Hilfe derer Tupel aus Wikidata extrahiert werden k\"onnen, zwischen denen korrespondierende Relationen bestehen.

	\subsubsection*{Query-Synthese und Ergebnisanfrage}
Basierend auf den gefundenen Stencils werden nun Filteranfragen auf Wikidata Query synthetisiert.\\
Die jeweils enthaltenen Pfade werden in ihre einzelnen Kanten zerlegt, Dependenzen reduziert (z.B. Entfernung doppelter Kanten oder unspezifischerer Pfade) und mittels Lookup in ihre korrespondierenden Wikidata Query Teilanfragen \"ubersetzt, bevor sie entsprechend ihrer Filtertypen (z.B. {\lq}inclusion/exclusion{\rq}) wieder zusammengef\"uhrt werden.\\
Diese Anfragen werden auf Wikidata Query ausgef\"uhrt und die Ergebnis-IDs gespeichert. Für jedes erhaltene Ergebnis muss wieder ein lokaler Graph aufgespannt werden.\\
Da allerdings nur Knoten und Kanten von Interesse sind, die auf die Schablone passen (und um redundante API-Aufrufe zu sparen), werden alle irrelevanten Kanten und Knoten entsprechend dem Stencil entfernt. Andererseits finden sich im Stencil auch ggf. Kanten, die nicht im lokalen Graphen enthalten sind. In diesem Fall werden die lokalen Graphen entsprechend erweitert.\\
Diesen Ergebnisgraphen fehlen teilweise noch Label-Informationen (Wikidata l\"ost benachbarte Knoten nur bis zur alphanumerischen ID auf), weswegen diesen Graphen in einem letzten Schritt menschenlesbare Informationen hinzugef\"ugt werden.\\
Nach dem dieser Schritt für alle Ergebnisgraphen erfolgt ist, liegen alle potenziell zur Eingabe passenden Graphen vor. Diese müssen dem Nutzer  nur noch visuell als Tupel dargestellt werden.

	\subsubsection*{Entfernen fehlerhafter Ergebnisse}
Da REx bei mehreren Schablonen und Pfadkombinationen nicht wissen kann, welche die vom Nutzer beabsichtigte Beziehung beschreibt, muss nun vom Nutzer eine Auswahl der falschen Ergebnisse erfolgen.\\
Dabei können die Ergebnistupel in der GUI markiert werden. Nachdem ein oder mehrere falsche Tupel-Einträge markiert sind, kann ein neuer Durchlauf gestartet werden.\\
Dieser unterscheidet sich dahingehend, dass die zuvor ermittelten Schablonen ohne diejenigen, welche falsche Tupel generieren, wiederverwendet werden k\"onnen.\\
Dieser Bereinigungsschritt kann solange wiederholt werden, bis die Ausgabe korrekt ist oder all Tupel entfernt wurden. Letzteres würde darauf hindeuten, dass die gewünschte Beziehung des Eingabe Tupel nicht in Wikidata abgebildet ist.

	\section{Programmierumgebung}
Das Projekt und die zugeh\"orige GUI wurde in C\# in Visual Studio entwickelt. Das entstandene Programm ist somit nur unter Windows einsetzbar (.NET Framework muss installiert sein). Als externe Frameworks/Tools wurden Wikidata und Wikidata Query verwendet. Da keine Daten lokal angelegt werden, muss eine Internetverbindung bestehen, um auf Wikidata und Wikidata Query API-Aufrufe ausf\"uhren zu können.

	\section{Architektur}
Das entstandene Programm ist in die vier nachfolgenden Architekturbereiche eingeteilt: UI, Core, API und Graph.\\
Um REx ggf. auch auf weitere Datenquellen anwenden zu k\"onnen, wurden alle Komponenten direkt f\"ur den Algorithmus relevanten Komponenten abstrahiert. So m\"ussen im Falle einer Neuanwendung lediglich die Klassen im API-Bereich angepasst werden.\\
\begin{center}
\includegraphics[width=0.5\textwidth]{Images/Architektur_grob.pdf}
\end{center}

	\subsection{Graph}
In diesem Bereich befinden sich alle Klassen, welche für die Graph-Repräsentation der Daten notwendig sind.\\
In Wikidata liegen die Daten zwar bereits in dieser Form vor, jedoch k\"onnte man andere Strukturen, wie z.B. DBpedia\footnote{\url{http://wiki.dbpedia.org/about}} oder andere Knowledge Graphen, ebenfalls in diese Strukturen \"uberf\"uhren, da stets ID und meist Label vorhanden sind.\\

\begin{tabularx}{\textwidth}{ |l|X| }
\hline 
\textbf{Klasse} & \textbf{Beschreibung} \\ 
\hline 
Node.cs & Stellt einen Knoten in einem Graphen dar. (ID, Label) \\ 
\hline 
Edge.cs & Stellt eine Kante in einem Graph dar. (ID, Label, Start, Ziel) \\ 
\hline 
Graph.cs & Beschreibt einen Graphen der aus Knoten und Kanten besteht. \\ 
\hline 
GraphUtils.cs & (Statische) Klasse, die notwendige Operationen auf Graphen (z.B. Multi-Join) erm\"oglicht. \\ 
\hline 
GraphBindings.cs & Aufbereitung der Graphen-Klasse für eine einfache Darstellung in einer XAML-UI. \\ 
\hline 
Path.cs & Beschreibt einen Pfad bestehend aus geordneten Kanten. \\ 
\hline 
\end{tabularx} 
	\subsection{API}
Im Bereich der API sind alle Klassen abgelegt, welche einfache Zugriffe auf die APIs Wikidatas und Wikidata Querys realisieren. Weiterhin sind hierin Klassen zur Synthese Wikidata Query spezifischer Filteranfragen enthalten.\\

\begin{tabularx}{\textwidth}{ |l|X| }
\hline 
\textbf{Klasse} & \textbf{Beschreibung} \\ 
\hline 
WikiData.cs & Klasse für alle direkten API-Anfragen an Wikidata. \\ 
\hline 
WikiDataQuery.cs & Schnittstelle für API-Anfragen an Wikidata Query. \\ 
\hline 
Query.cs & Klasse, mit der HTTP-Anfragen jeglicher Art durchgef\"uhrt werden. \\ 
\hline 
WikiDataQueryFilter.cs & Repr\"asentation der Stencils als Filter der Wikidata Query. \\
\hline
\end{tabularx}

	\subsection{Core}
Der Bereich Core beinhaltet alle f\"ur den REx-Algorithmus relevanten Komponenten.\\

\begin{tabularx}{\textwidth}{ |l|X| }
\hline 
\textbf{Klasse} & \textbf{Beschreibung} \\ 
\hline 
SynthTask.cs & Koordiniert den gesamten Ablauf der Synthese und Filterung. Ein SynthTask kann in Teilen gestartet werden, sodass z.B. im Bereinigungsschritt ein neuer SynthTask verwendet werden kann, der Informationen seines Vorg\"angers enth\"alt. \\ 
\hline 
SynthEngine.cs & Stellt Funktionen für die Relationsextraktion, die Filtersynthese und weitere Schritte der Synthese bereit. \\ 
\hline 
SynthResult.cs & Klasse, welche ein synthetisiertes Ergebnisse mitsamt dessen Metadaten (Stencil-ID etc.) verwaltet und die Tupel-Darstellung in der UI vereinfacht. \\ 
\hline 
\end{tabularx}

	\subsection{UI}
\begin{tabularx}{\textwidth}{ |l|X| }
\hline 
\textbf{Klasse} & \textbf{Beschreibung} \\ 
\hline 
MainWindow.xaml & Beschreibung der UI als WPF. \\ 
\hline 
MainWindow.xaml.cs & Programmcode für die UI. Startpunkt für die Anwendung. \\ 
\hline 
\end{tabularx}\\

Die Verbindungen der einzelnen Bereiche sind in der nachfolgenden Grafik dargestellt:

\includegraphics[width=\textwidth]{Images/CodeMap.pdf}

	\subsection{Sequenzdiagramm}
In der nachfolgenden Grafik ist der Ablauf der Synthese schematisch abgebildet.\\
Nach dem initialen Erstellen eines SynthTasks wird ein SynthEngine gestartet und von der UI aus die getSynthResults Methode ausgef\"uhrt. Ihr werden die Freitext-Elemente des Eingabetupels mitgegeben.\\
Die darauf folgenden Prozesse werden, wie oben beschrieben, durchgef\"uhrt. Der Bereinigungs-Schritt unterscheidet sich zu einem normalen getSynthResults Aufruf nur in dem Punkt, dass die Schablonen nicht neu gesucht werden, sondern die alten (abzüglich der falschen Schablonen) als Startpunkt verwendet werden.\\

\includegraphics[width=\textwidth]{Images/Sequenzdiagramm.pdf}

	\section{Verwendung}
Im Nachfolgenden wird ein Beispieldurchlauf (u.a. mit Bildschirmaufnahmen) gezeigt, um die tats\"achliche Interaktion des Nutzers mit REx zu zeigen.\\
Das gewünschte Eingabetupel wird in die obere Suchbox eingegeben. Dabei müssen die einzelne Einträge durch ein Pipeline Symbol (|) getrennt werden. Eine Trennung mittels Komma ist nicht möglich, da es zahlreiche Items gibt, die ein Komma im Titel haben.\\
\begin{center}
\includegraphics[width=0.8\textwidth]{Images/Eingabe.PNG}\\
\end{center}

Nach der Eingabe wird der Algorithmus mit der Return-Taste gestartet.\\
Solange der Synthesevorgang läuft, kann in den Log-Reiter gewechselt werden, um im Detail zu beobachten, in welchem Schritt sich der Vorgang derzeit befindet.\\
Nachdem die Bearbeitung beendet ist, werden im Results-Reiter die ersten drei Ergebnisgraphen pro Schablone als Tupel dargestellt. (Da die zu durchsuchende Datenbasis sehr groß ist, kann die Verarbeitung durchaus länger als eine Minute dauern.)\\
Sobald die Ergebnisse angezeigt werden, können falsche Einträge mittels Doppelklick markiert bzw. demarkiert werden. Es ist ebenfalls m\"oglich mehrere falsche Ergebnisse zu markieren.\\

\begin{center}
\includegraphics[width=0.6\textwidth]{Images/Markierte_Eintraege.PNG}\\
\end{center}

Nachdem alle Markierungen getätigt wurden, kann der Bearbeitungsvorgang durch einen Klick auf den Button {\lq}Apply Changes{\rq} gestartet werden.\\
Ist die Bearbeitung beendet, so können wieder falsche Einträge markiert werden. Diese Prozedur kann solange wiederholt werden bis keine falschen Einträge mehr auftauchen.\\
Bei jedem Bereinigungsdurchlauf kann im Log nachvollzogen werden, wie viele falsche Schablonen entfernt wurden.\\

\begin{center}
\includegraphics[width=0.5\textwidth]{Images/entfernte_Stencils.PNG}
\end{center}

	\section{Weitere Verbesserungen}
In der aktuellen Version gibt es noch Features, die f\"ur einen produktiven Einsatz REx w\"unschenswert bzw. dank des Filter-Reichtums Wikidata Querys noch m\"oglich w\"aren.\\
So stellt das aktuelle Programm bisher nicht alle gefundenen Graphen dar, auch wenn diese im Hintergrund vorhanden sind. Die UI m\"usste so erweitert werden, dass die anzuzeigenden Graphen und die darin enthaltenen Tupel dynamisch nachgeladen werden.\\
Zusätzlich zur Anzeige wäre auch eine Export Funktion in eine externe Datei denkbar, um die gefundenen Daten für eine Weiterverarbeitung speichern zu können.\\
Bei der Verarbeitung der Daten stellt sich jedoch ein bekanntes Problem dar: Die Anfragen auf Wikidata sind begrenzt\footnote{\url{https://www.wikidata.org/w/api.php?action=help&modules=wbgetentities}}. Will man sehr viele Anfragen mit mehr als 50 Entit\"aten tätigen, so muss man sich auf Wikidata als Bot registrieren. Dies wurde in dem Praktikum nicht getan, da es nicht direkt mit der Funktionalität des Programmes zusammenhängt. Derzeit werden gr\"o{\ss}ere Anfragen daher automatisch in Kleinere aufgeteilt und zeitlich versetzt verarbeitet.\\
Eine weiteres Bearbeitungsgebiet ist die Performanz der Anwendung. Da es sich um den ersten Entwicklungsdurchlauf handelt, in dem die generelle Umsetzbarkeit im Vordergrund steht, wurde nicht allzu sehr auf die Performanz geachtet. So k\"onnte eine andere Struktur zur Verwaltung der Graph-Datenstruktur bereits Verbesserungen zeigen.\\
Seitens des Algorithmus selbst, k\"onnen noch wesentlich feinere Filter implementiert werden. Anstelle ganzer Stencils auszufiltern, k\"onnten beispielsweise einzelne Pfade oder sogar Knoten und Kanten betrachtet werden und explizit ausgefiltert werden. Die Filter-Klassen bieten diese Funktionalit\"at bereits an, sind jedoch noch nicht in diesem Umfang in den Rest der SynthEngine integriert.\\
Weiterhin bietet Wikidata Query unter anderem Filter, die es erm\"oglichen nach Zeit und sogar nach konkreten geographischen Koordinaten zu suchen. Ergebnistupel k\"onnten so beispielsweise auf eine bestimmte Stadt zu einer bestimmten Epoche eingegrenzt werden.\\
Obwohl diese Funktionen noch nicht im Programm implementiert sind, ist es mit Sicherheit m\"oglich sie in die derzeitige Architektur einzubinden.

	\section{Fazit}
Trotz einiger Einschr\"ankungen ist im Rahmen des Fortgeschrittenen-Praktikums eine Software entstanden, welche komplexe Relationen zwischen Entit\"aten einer technisch unbegrenzten Dom\"ane extrahieren kann und anhand dieser neue Informationen auffinden kann.\\
Selbst in der derzeitigen Version erm\"oglicht REx es also Nutzern aller Wissensst\"ande, anhand simpler Interaktionen Daten praktischen Nutzens zu extrahieren ohne auch nur eine Datenbankanfrage programmieren zu m\"ussen.

\end{document}